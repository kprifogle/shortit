Rails.application.routes.draw do
  root to: 'welcome#index'
  resources :links, only: [:create, :show]
  get ':action' => 'welcome#:action'
end
