require 'rails_helper'

RSpec.describe Link, type: :model do
  context :encode do
    
    it 'encodes' do
      link = Link.create(long_url: 'www.google.com')
      link.encode
      expect(link.short_url).to eq('b')
      expect(Link.decode(link.short_url)).to eq('www.google.com')
    end

    it 'avoids bad words' do
      bad_id = Link.decode_id(Link::OFF_LIMITS.first)
      link = Link.create(long_url: 'test', id: bad_id)
      link.encode
      expect(link.short_url).to eq('foq' + Link::SPECIAL_CHARACTER * 2)
      expect(Link.decode_id('foq' + Link::SPECIAL_CHARACTER * 2)).to eq(bad_id)
    end

    it 'avoids duplicate ids' do
      bad_id = Link.decode_id(Link::OFF_LIMITS.first)
      link = Link.create(long_url: 'test', id: bad_id)
      link2 = Link.create(long_url: 'test2', id: bad_id + 1)
      link.encode
      link2.encode
      expect(link.short_url).to eq('foq' + Link::SPECIAL_CHARACTER * 2)
      expect(link2.short_url).to eq('foq' + Link::SPECIAL_CHARACTER)
    end

  end

  context :increment do
    it 'increments' do
      link = Link.create(long_url: 'test')
      link.increment(:count, by = 1)
      expect(link.count).to eq(1)
    end
  end

end