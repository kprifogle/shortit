require 'spec_helper'

describe LinksController do
  render_views

  context 'create' do
    it 'can encode' do
      xhr :post, :create, link: {long_url: "www.google.com", :encode => true}
      expect(Link.count).to eq(1)
      expect(Link.first.long_url).to eq("www.google.com")
      expect(Link.first.short_url).to eq("b")
    end

    it 'can decode' do
      link = FactoryGirl.create(:link, :long_url => "www.google.com")
      link.encode
      xhr :post, :create, link: {short_url: link.short_url, :decode => true}
      expect(assigns(:long_url)).to eq("www.google.com")
      expect(Link.first.long_url).to eq("www.google.com")
    end

    it 'indifferent to domain and protocol' do
      link = FactoryGirl.create(:link, :long_url => "www.google.com")
      link.encode
      xhr :post, :create, link: {short_url: "http://test.host/" + link.short_url, :decode => true}
      expect(assigns(:long_url)).to eq("www.google.com")
      expect(Link.first.long_url).to eq("www.google.com")
    end

    it 'short url not found' do
      xhr :post, :create, link: {short_url: "e", :decode => true}
      expect(assigns(:flash)).to eq("Short link not found")
    end
  end


end