module ApplicationHelper
  def append_icon(text, icon)
    (text + "<i class='fa fa-#{icon}' style='padding-left:10px;'></i>").html_safe
  end
end
