class WelcomeController < ApplicationController

  def index
    @reverse = params[:reverse]
  end

  def action_missing(action_name)
    link = Link.find_by_short_url(action_name).long_url
    link.increment(:count, by = 1)
    link = "http://" + link if !link.include?("http")
    redirect_to link, status: 302
  rescue NoMethodError
    flash[:alert] = "This shortened link does not exist"
    redirect_to root_path
  end

end