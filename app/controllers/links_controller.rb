class LinksController < ApplicationController
  require "net/http"

  def create
    if !link_params[:long_url].blank? && link_params[:encode]
      @link = Link.find_by_long_url(link_params['long_url']) || Link.create(link_params)
      @link.encode
      @short_url = [request.protocol + request.host_with_port, @link.short_url].join('/')
    elsif !link_params[:short_url].blank? && link_params[:decode]
      path = URI.parse(link_params[:short_url]).path.gsub("/","")
      @long_url = Link.decode(path)
      @flash = 'Short link not found' if @long_url.blank?
    end
  end

  private

  def link_params
    params.require(:link).permit([:long_url, :short_url, :encode, :decode])
  end

end