class Link < ActiveRecord::Base
  attr_accessor :encode, :decode

  LIBRARY = Array('a'..'z') + Array('A'..'Z') + Array('0'..'9')
  TRANSCRIPTION_ERRORS = {'0' => 'O', '1' => 'l'}
  OFF_LIMITS = ['foo', 'fop', 'bar', 'f']
  SPECIAL_CHARACTER = '$'

  def self.transcribe(string)
    string.gsub(/./){|n|TRANSCRIPTION_ERRORS[n] || n}
  end

  @@alphabet = self.transcribe(LIBRARY.join('')).split('').uniq

  def get_short_url(i=nil)
    i ||= id
    return @@alphabet[0] if i == 0
    s = ''
    base = @@alphabet.length
    while i > 0
      s << @@alphabet[i.modulo(base)]
      i /= base
    end
    s.reverse
  end

  def encode
    short = get_short_url
    disp = 0
    while OFF_LIMITS.include?(short.gsub(SPECIAL_CHARACTER,''))
      disp += 1
      short = get_short_url(id + disp) + (SPECIAL_CHARACTER * disp)
    end
    self.short_url = short
    save
  end

  def self.decode(string)
    Link.find_by_short_url(Link.transcribe(string)).try(&:long_url)
  end

  def self.decode_id(string)
    i = 0
    base = @@alphabet.length
    string = Link.transcribe(string)
    string.each_char { |c| c == Link::SPECIAL_CHARACTER ? (i-=1) : i = i * base + @@alphabet.index(c) }
    i
  end

end